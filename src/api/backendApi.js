let host = "http://localhost:8000";

export const Apis = {
  UserLogin: host + "/api/user/login",
  GetUsers: host + "/api/user",
  GetCourse: host + "/api/user/course",
  CreateCourse: host + "/api/user/course",
  CreateLecture: host + "/api/user/course/",
};
